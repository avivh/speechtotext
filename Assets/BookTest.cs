﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class BookTest : MonoBehaviour
{
    public TMP_Text txtResult;
    public TMP_Text txtInstructions;
    public TMP_Text txtStatus;
    public string[] words;
    private int wordIndex;
    VoiceManager vm;
    public float seconds = 3;
    public float currentTime;

    int restarting = 0;
    private void Start()
    {
        vm = GetComponent<VoiceManager>();
        wordIndex = 0;
        currentTime = Time.time;
        StartListening();

    }

    private void Update()
    {

        txtInstructions.text = "Say: " + words[wordIndex];

      if (Time.time > currentTime + seconds)
        {
           // StartListening();
            currentTime = Time.time;
        }

        if (words[wordIndex] == vm.speechResult) NextWord();
    }


    void StartListening()
    {
     //   vm.StopListening();
    //   vm.StartListening();
        restarting++;
        txtStatus.text = "restarting "+ restarting;
    }
    private void NextWord()
    {
        txtResult.text += words[wordIndex];
        vm.StartSpeaking(words[wordIndex]);
        wordIndex++;
       // StartListening();



    }

}
