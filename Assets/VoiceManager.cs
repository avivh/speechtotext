﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TextSpeech;
using TMPro;
using UnityEngine.Android;
using UnityEngine.Events;

public class VoiceManager : MonoBehaviour
{
    const string LANG_CODE = "en-US";
    public TMP_Text uiText;

    public UnityEvent finalSpeechResult; 
    public UnityEvent partialSpeechResult;

    public string speechResult;

    public bool isListenning = false;
    

    private void Start()
    {
        Setup(LANG_CODE);
#if UNITY_ANDROID
        SpeechToText.instance.onPartialResultsCallback = OnPartialSpeechResult;
#endif
        SpeechToText.instance.onResultCallback = OnFinalSpeehResult;

        TextToSpeech.instance.onStartCallBack = OnSpeakStart;
        TextToSpeech.instance.onDoneCallback = OnSpeakStop;
        CheckPermission();
    }

    void Setup(string code)
    {
        SpeechToText.instance.Setting(code);
        TextToSpeech.instance.Setting(code, 1, 1);

    }

    void CheckPermission()
    {
#if UNITY_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#endif
    }


    private void Update()
    {
        
    }


    #region Text To Speech
    public void StartSpeaking (string message)
    {
        TextToSpeech.instance.StartSpeak(message);
    }

    public void StopSpeaking()
    {
        TextToSpeech.instance.StopSpeak();
    }

    void OnSpeakStart()
    {
        Debug.Log("Strting to Speek");
    }

    void OnSpeakStop()
    {
        Debug.Log("Stopped Speeking");
    }

    #endregion
    #region Speech To Text
    public void StartListening()
    {
        SpeechToText.instance.StartRecording();
    }

    public void StopListening()
    {
      //  SpeechToText.instance.StopRecording();
    }

    void  OnFinalSpeehResult(string result)
    {
        speechResult = result;
        uiText.text = result;

    }

    void OnPartialSpeechResult(string result)
    {
        speechResult = result;
        uiText.text = result;
    }
    #endregion

}
