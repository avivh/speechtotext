﻿using UnityEngine;

public class UnityAnswers : MonoBehaviour
{
    void Start()
    {
        // Whenever Unity Answers, call QuestionAnswered.
        OnUnityAnswers += QuestionAnswered;

        // Answer the question.
        RaiseAnswer("Great, someone made you a script example.");
        RaiseAnswer("Now, read the comments and play around with it.");
    }

    // Note: None of the following has to be 'public', they could also be 
    // 'private', 'protected', 'internal' or 'protected internal'.
    // But, let's keep it simple.    

    // This functions signature matches the callback signature! (Important)
    // Has: Return of type 'void' and 1 parameter of type 'string'
    public void QuestionAnswered(string message)
    {
        // Some dummy example code.
        Debug.Log(message);
    }

    // Callback signature
    // Has: Return of type 'void' and 1 parameter of type 'string'
    public delegate void AnswerCallback(string message);

    // Event declaration
    public event AnswerCallback OnUnityAnswers;

    // Calls, "raises" or "invokes" the event. Note that if no one subscribed
    // to the event, the event will be null. We need to check this first to
    // prevent errors.
    public void RaiseAnswer(string message)
    {
        if (OnUnityAnswers != null)
            OnUnityAnswers(message);
    }
}