﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems; // Required when using Event data.

public class Clicker : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent downEvent;
    public UnityEvent upEvent;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("down");
        downEvent.Invoke();
       
    }   

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("uo");
        upEvent.Invoke();
    }
}
